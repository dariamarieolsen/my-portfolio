'use strict'

const btnStartAgain = document.getElementById("start-again")
const imgPlayer = document.getElementById("img-player")
const imgComputer = document.getElementById("img-computer")
const scorePlayer = document.getElementById("score-player")
const scoreComputer = document.getElementById("score-computer")
const textmessage = document.getElementById("outcome")
const chooseWeapon = document.querySelector(".choose-header")



const optionsArr = ["images/rock.jpg", "images/paper.jpg", "images/scissors.jpg"]


const rock = optionsArr[0]
const paper = optionsArr[1]
const scis = optionsArr[2]

// Game logic function
let pScore = 0
let cScore = 0
function logic(imgPlayerSrc, imgComputerSrc) {

    if (imgPlayerSrc === imgComputerSrc) { // lika
        scorePlayer.textContent = pScore;
        scoreComputer.textContent = cScore;
        textmessage.textContent = "It is a tie";

    } else if (
        (imgPlayerSrc === scis && imgComputerSrc === paper) || // score til player
        (imgPlayerSrc === rock && imgComputerSrc === scis) ||
        (imgPlayerSrc === paper && imgComputerSrc === rock)
    ) {


        if (pScore < 3) {
            pScore++
            scorePlayer.textContent = pScore
            textmessage.textContent = "1 point to player";
        }
        if (pScore === 3) {
            textmessage.textContent = "Player won the game";
            victory();
        }

    } else if (
        (imgComputerSrc === scis && imgPlayerSrc === paper) || // score til computer
        (imgComputerSrc === rock && imgPlayerSrc === scis) ||
        (imgComputerSrc === paper && imgPlayerSrc === rock)
    ) {

        if (cScore < 3) {
            cScore++;
            scoreComputer.textContent = cScore;
            textmessage.textContent = "1 point to computer";
        }
        if (cScore === 3) {
            textmessage.textContent = "Computer won the game";
            victory()
        }
    }
}

function getComputerChoice() {
    const computerIndex = Math.floor(Math.random() * optionsArr.length);
    return optionsArr[computerIndex];
}

document.getElementById("rock").addEventListener("click", function () {
    imgPlayer.src = optionsArr[0]
    const computerChoice = getComputerChoice();
    imgComputer.src = computerChoice
    logic(rock, computerChoice)

})

document.getElementById("paper").addEventListener("click", function () {
    imgPlayer.src = optionsArr[1]
    const computerChoice = getComputerChoice();
    imgComputer.src = computerChoice
    logic(paper, computerChoice)

})

document.getElementById("scissors").addEventListener("click", function () {
    imgPlayer.src = optionsArr[2]
    const computerChoice = getComputerChoice();
    imgComputer.src = computerChoice
    logic(scis, computerChoice)

})

function victory() {
    document.querySelector(".game-container").classList.add("victory");
    btnStartAgain.hidden = false;
    chooseWeapon.hidden = true;
}


btnStartAgain.addEventListener("click", function () {
    pScore = 0
    cScore = 0
    scorePlayer.textContent = 0
    scoreComputer.textContent = 0
    textmessage.textContent = "Outcome..."
    imgPlayer.src = "images/question-mark-icon.svg"
    imgComputer.src = "images/question-mark-icon.svg"
    btnStartAgain.hidden = true
    chooseWeapon.hidden = false
    document.querySelector(".game-container").classList.remove("victory")
})


// connecting page 1 with page 2

const playerName = localStorage.getItem('playerName')
document.getElementById("player-name").textContent = playerName;


