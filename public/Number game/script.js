'use strict'

// Guess a number between 1-20
// id = Number holdes a random number
//when i type in a number --> the number is eather correct, to high or to low, and it stands in outcome-text
//for each guessing, score goes down
//when guessed right number, display number 
//when restart, everything goes back but not highscore
//if score in next round is better (high === good), the new highscore is score. If not, highscore stay the same as in previous round.  


// Making variables to each element in html
const container = document.getElementById("game-container")
const againBtn = document.getElementById("again")
const checkBtn = document.getElementById("check")
const highscore = document.getElementById("highscore")
const message = document.getElementById("message")
const originScore = document.getElementById("score")

const secretNumber = Math.floor(Math.random() * 21); // 21 because the last does not count

let score = 20; //score will decrease by every click only when score is outside the eventlistener

checkBtn.addEventListener("click", function() {
    const guess = Number(document.getElementById("guess").value)

if(!guess) {
    message.textContent = "No number"
    score -= 1
    originScore.textContent = score
} else if (guess === secretNumber) {
    message.textContent = "Correct Number"
    container.style.backgroundColor = "#90ee90"
    document.getElementById("number").textContent = secretNumber


} else if (guess < secretNumber) {
    if (score > 1) {
    message.textContent = "Too low number"
    score --
    originScore.textContent = score
    } else {
    message.textContent = "You lost the game"
    originScore.textContent = 0

    }
} else if (guess > secretNumber) {
    if (score > 1){
    message.textContent = "Too high number"
    score --
    originScore.textContent = score
    } else {
        message.textContent = "You lost the game"
        originScore.textContent = 0
    }
}

})

againBtn.addEventListener("click", function() {
    
    container.style.backgroundColor = "#fafad2"
    highscore.textContent = score
    originScore.textContent = 20
    document.getElementById("number").textContent = "?"
    guess.value = ''
})