// Recepies
let recipesArr = []
const container = document.querySelector(".recipes-container")


fetch("/recipes") //chrome spør etter recipes, og server svarer med å sende det som finnes der
    .then(res => res.json())
    .then(jsonData => {
        recipesArr = jsonData.recipes
        console.log(recipesArr)
        gettingData()
    })
    .catch((error) => {
        console.error("Error", error)
    })


function gettingData() {
    container.innerHTML = "" // slik at recept ikke repiterer seg ved hver klikk
    recipesArr.forEach((recipe, index) => {

        // outer container
        const individContainer = document.createElement("div")
        individContainer.className = "index-con"
        individContainer.setAttribute("individ-con", index)
        container.appendChild(individContainer)
    })

    carousel() //bla gjennom recipe arr
}



//Carousel
let currIndex = 0
const btnNextC = document.getElementById("btn-next-c");
const btnPrevC = document.getElementById("btn-prev-c");

function carousel() {

    btnNextC.addEventListener("click", function () {
        if (currIndex < recipesArr.length - 1) { //slik at den ikke går over arrayen
            currIndex++
        } else {
            currIndex = 0 // starter på 0 igjen etter en "loop"
        }
        carouselContent()
    })

    btnPrevC.addEventListener("click", function () {
        if (currIndex < recipesArr.length && currIndex > 0) {
            currIndex--;
        } else {
            currIndex = recipesArr.length - 1
        }
        carouselContent()
    })

    carouselContent()
}

function carouselContent() {
    const contentContainer = document.querySelector(".index-con");
    contentContainer.innerHTML = "";
    const recipe = recipesArr[currIndex];

    const recName = document.createElement("h3");
    recName.className = "recname";
    recName.innerHTML = `${recipe.name}`;

    const recImg = document.createElement("img");
    recImg.className = "recimg";
    recImg.setAttribute("src", recipe.img);
    recImg.setAttribute("alt", "picture of food");

    const upperContainer = document.createElement("div");
    upperContainer.className = "upperContainer";
    upperContainer.appendChild(recName);
    upperContainer.appendChild(recImg);


    const howToInformation = document.createElement("div");
    howToInformation.className = "how-to-section";
    howToInformation.innerHTML =
        `<h5>Procedur <span class="plus-sign" id="plusSign" onclick="toggleRecipeInformation()"> + </span></h5>`;

    const toggleContainer = document.createElement("div");
    toggleContainer.className = "toggle-container";

    const ingredients = recipe.ingredients;
    const ingrDiv = document.createElement("div");
    ingrDiv.className = "ingredients";
    ingrDiv.innerHTML = `<h5 class="rec-header"> Ingredienser </h5>`;

    ingredients.forEach(ingr => {
        if (!ingr.preparation) {
            ingrDiv.innerHTML += `<p class="name">${ingr.name}: ${ingr.quantity}</p>`;
        } else {
            ingrDiv.innerHTML += `<p class="name">${ingr.name}: ${ingr.quantity}, ${ingr.preparation}</p>`;
        }
    });

    const instr = recipe.instructions;
    const instrCon = document.createElement("div");
    instrCon.className = "instrCon";
    instrCon.innerHTML = `<h5 class="rec-header"> Instructioner </h5>`;

    for (let i = 0; i < instr.length; i++) {
        instrCon.innerHTML += `
            <div class="inner-instr">
                <li class="instr-index">${i + 1}: </li> 
                <li class="instr-li">${instr[i]}</li>
            </div>
        `;
    }

    // Sub-btn for the how-to section
    const subButtonHowTo = document.createElement("button");
    subButtonHowTo.type = "button";
    subButtonHowTo.className = "sub-btn";
    subButtonHowTo.innerHTML = `<img src="images/delete-icon.png" alt="delete-icon" id="delete-icon">`;
    howToInformation.appendChild(subButtonHowTo);

    subButtonHowTo.addEventListener("click", async function () {
        const recIndex = this.getAttribute("individ-con");
        container.removeChild(contentContainer);

        const response = await fetch(`/recipes/${recIndex}`, { method: 'DELETE' });
        const data = await response.json();

        if (data.success) {

            gettingData();
        } else {
            console.error(data.message);
        }
    });

    // Append elements to container
    contentContainer.appendChild(upperContainer);
    contentContainer.appendChild(howToInformation);
    toggleContainer.appendChild(ingrDiv);
    toggleContainer.appendChild(instrCon);
    contentContainer.appendChild(toggleContainer);
}


function toggleRecipeInformation() {
    const content = document.querySelector('.toggle-container');
    const plusSign = document.getElementById('plusSign');
    const section = document.querySelector('.how-to-section');

    content.classList.toggle("active");
    plusSign.innerHTML = content.classList.contains('active') ? '-' : '+';
    section.style.borderBottom = content.classList.contains('active') ? 'none' : '0.0625rem solid #B5B1B1';

    const upperContainer = document.querySelector(".upperContainer");
    upperContainer.classList.toggle("toggle-upperContainer", content.classList.contains('active'));


    btnPrevC.classList.toggle("hideArrow", content.classList.contains('active'));
    btnNextC.classList.toggle("hideArrow", content.classList.contains('active'));
}

document.querySelector(".goBack").addEventListener("click", function () {
    window.location.href = '/index.html';
})


