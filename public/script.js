// Jeg liker å etterligne andres design og teste de ut selv. 
//Dette prosjektet var ment til å bli min portfolio, men det ble ikke så. 
// Jeg legger vekt på funksjonalitet framfor design i mine småprosjekt.



// set height of the svg path as constant
const svg = document.getElementById("svg");
const path = document.getElementById("path");

const scroll = () => {
    const svgRect = svg.getBoundingClientRect(); //position and dimensions of the SVG element within the viewport.

    const distance = window.scrollY; //scroll distance from the top of the page

    const startDrawingAt = (svgRect.top + window.scrollY) - 100; //calculates the absolute distance from the top of the document to the top of the SVG element, taking into account the current scroll position.
    const totalDistance = svg.clientHeight - window.innerHeight; // calculates height of SVG and eliminates everything else 

    const percentage = Math.min(1, Math.max(0, (distance - startDrawingAt) / totalDistance));

    const pathLength = path.getTotalLength();

    path.style.strokeDasharray = `${pathLength}`;
    path.style.strokeDashoffset = `${pathLength * (1 - percentage)}`; // so it dont goes backwards
};

scroll();
window.addEventListener("scroll", scroll);

//Curtains


const curtains = document.querySelector(".curtain-container");

function curtainFunction() {

    const options = {
        root: null,
        threshold: 0,
        rootMargin: "-20px",
    };

    const observer = new IntersectionObserver(function (entries) {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                entry.target.classList.add("active");
            }
        });
    }, options);

    observer.observe(curtains);
}

function curtainsScroll() {

    if (window.scrollY < 10) {
        curtains.classList.remove("active");
    }
}

curtainFunction();

window.addEventListener("scroll", curtainsScroll);

