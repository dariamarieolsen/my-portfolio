const fs = require("fs"); //fs = filesystem
const port = 3000

const express = require('express') // tar i bruk express package jeg laddet ned
console.log('startade server. port: ', port)
const app = express() // applikasjon som lar oss sette opp en server
app.use(express.urlencoded())

app.use(express.static('public')) //skal sende ut filer i public til klienten(webleser, chrome)


const recipes = require('./public/recipes.json')

async function saveRec() { // til json fil
    let filehandle = null;
    let prom = null;

    try {
        // Creating and initiating filehandle
        filehandle = await fs.promises.open('./public/recipes.json', 'w');
        // Writing the file by using
        // write() method
        prom = filehandle.writeFile(JSON.stringify(recipes, null, "  "));

    } finally {

        if (filehandle) {

            // Display the result
            prom.then(function (result) {

            })
            // Close the file if it is opened.
            await filehandle.close();
        }
    }
}
//jeg åpner mitt prosjekt med liveserver, chrome sender deretter en request til server inne i VS (leser path og om filen finnes), og om den gjør det vises den i chrome. 

//setter opp en rute gjennom .get/post osv. Første parameter er path, "/" er root-path feks.

app.post('/addrecipe', async function (req, res) { //funksjonen som sendes recipes fra chrome til server, via addrecipe vei
    //req = request
    //chrome sender get eller post req, og all data er i req.body

    const ingredients = req.body.ingr
    const ingrParts = ingredients.split('-')
    const ingrPartsFun = ingrParts.map(part => {
        const [name, quantity, preparation] = part.split(',');
        const capitalizedName = name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
        return { name: capitalizedName, quantity, preparation };
    });

    const instructions = req.body.instr
    const instrParts = instructions.split(',');
    const instrPartsFun = instrParts.map(parts => {
        return parts.charAt(0).toUpperCase() + parts.slice(1).toLowerCase();;
    });


    const formObject = {
        name: req.body.recname,
        img: req.body.imgurl,
        ingredients: ingrPartsFun,
        instructions: instrPartsFun
    };



    recipes.recipes.unshift(formObject);
    console.log(recipes)
    await saveRec()

    res.redirect('/recipes/recipes.html');

})


app.get("/recipes", function (req, res) { // funksjonen som sendes recipes ut av server til chrome
    res.json(recipes);
})


app.delete('/recipes/:id', async function (req, res) {
    const recipeId = parseInt(req.params.id);

    // Remove the recipe with the given ID from the recipes array
    const deletedRecipe = recipes.recipes.splice(recipeId, 1);

    // Save the updated recipes array to the JSON file
    await saveRec();

    res.status(200).json({ success: true, message: 'Recipe deleted successfully', recipe: deletedRecipe });
});

app.listen(port)
